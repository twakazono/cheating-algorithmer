# Cheating Algorithmer
This is an API and its frontend to classify the problems of competitive
 programming.

## Features
- Classify problems with tags
- Predict difficulty
- Find Similar problems
- Japanese support (only classification)

## Installation
First of all, you need to install all the requirements.
```sh
# for the api
pip install -r requirements.txt
```
```sh
# for the frontend
cd frontend
npm install
npm run build
```

## Launch
By default, API runs on port 5000 and frontend runs on port 8080.
```sh
# api
python api.py
```
```sh
# frontend
node server.js
```

## Others
All the code I experimented with to create the models is in `notebooks/`


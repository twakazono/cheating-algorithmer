import ast

import numpy as np
import pandas as pd
from flask import Flask, jsonify, request
from flask_cors import CORS
# from janome.tokenizer import Tokenizer as Tokenizer_ja
from keras.models import load_model
from keras.preprocessing.sequence import sequence
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from problem_data import ProblemData

MAX_LEN = 512
MAX_FEATURES = 10000
LABELS = ['2-sat', 'binary search', 'bitmasks', 'brute force',
          'chinese remainder theorem', 'combinatorics',
          'constructive algorithms', 'data structures', 'dfs and similar',
          'divide and conquer', 'dp', 'dsu', 'expression parsing', 'fft',
          'flows', 'games', 'geometry', 'graph matchings', 'graphs', 'greedy',
          'hashing', 'implementation', 'interactive', 'math', 'matrices',
          'meet-in-the-middle', 'number theory', 'probabilities', 'schedules',
          'shortest paths', 'sortings', 'string suffix structures', 'strings',
          'ternary search', 'trees', 'two pointers']
LABELS_JA = ['dp', 'geometry', 'graphs']


class ModelData:
    def __init__(self, is_ja: bool, p_name: str, l_name: str, d_name=None):
        # somehow cannot use the model created in the other scope.
        # self.l_model = load_model(l_name)
        # self.d_model = None
        self.l_name = l_name
        self.d_name = d_name
        self.stdsc = None
        self.is_ja = is_ja

        if p_name.endswith('.tsv'):
            df = pd.read_csv(p_name, delimiter='\t')
        else:
            df = pd.read_csv(p_name)

        if d_name:
            df = df.dropna(subset=['difficulty'])
            y = df['difficulty'].values.reshape(-1, 1)
            self.stdsc = StandardScaler().fit(y)

        if not is_ja:
            self.problems = dict()
            for t, d, c, l, i in zip(df['tags'], df['difficulty'],
                                     df['contest'], df['letter'], df['title']):
                tags = ast.literal_eval(t)
                for tag in tags:
                    if tag in self.problems:
                        self.problems[tag].append(ProblemData(c, l, d, i))
                    else:
                        self.problems[tag] = [ProblemData(c, l, d, i)]

        x = df['main_text'].values
        x_train, _ = train_test_split(x, test_size=0.2, random_state=93187)

        self.tokenizer = Tokenizer(num_words=MAX_FEATURES)
        self.tokenizer.fit_on_texts(x_train)

    def load_l_model(self):
        return load_model(self.l_name)

    def load_d_model(self):
        return load_model(self.d_name)


model_en = ModelData(False, 'data_en/problems-all.tsv', 'models/label.h5',
                     'models/difficulty.h5')
model_ja = ModelData(True, 'data_jp/data_jp.csv', 'models/label_jp.h5')

base_url = '/api/v1'
predict_url = base_url + '/predict'
tags_url = base_url + '/tags'

api = Flask(__name__)
CORS(api)


@api.route(tags_url, methods=['GET'])
def tags_en():
    result = {
        'tags': LABELS,
    }
    return jsonify(result)


@api.route(tags_url + '/ja', methods=['GET'])
def tags_ja():
    result = {
        'tags': LABELS_JA,
    }
    return jsonify(result)


@api.route(predict_url, methods=['POST'])
def predict_en():
    l_pred, d_pred = predict(model_en)

    labels = [LABELS[int(l)] for l in l_pred][::-1]

    difficulty = int(d_pred)

    # print("--------------------")
    # print("labels: %s" % labels)
    # print("difficulty:", difficulty)
    # print("--------------------")

    related = dict()
    for i, l in enumerate(labels):
        r = get_related(model_en, l, difficulty)
        related[i] = {'c': r.contest, 'l': r.letter, 't': r.title}

    result = {
        'labels': labels,
        'difficulty': difficulty,
        'related': related,
    }

    return jsonify(result)


@api.route(predict_url + '/ja', methods=['POST'])
def predict_ja():
    l_pred, _ = predict(model_ja)
    label = LABELS_JA[int(l_pred[-1])]

    # print("--------------------")
    # print("label: %s(id: %d)" % (label, int(l_pred)))
    # print("--------------------")

    result = {
        'label': label,
        'difficulty': -1,
    }

    return jsonify(result)


def predict(model_data: ModelData):
    data = request.json
    text = data['text']

    # if model_data.is_ja:
    #     tokenizer_ja = Tokenizer_ja(wakati=True)
    #     x = [t for t in tokenizer_ja.tokenize(text)]
    x = model_data.tokenizer.texts_to_sequences([text])
    x = sequence.pad_sequences(x, maxlen=MAX_LEN)

    l_pred = None
    d_pred = None

    l_model = model_data.load_l_model()
    l_pred = l_model.predict(x)

    l_pred = l_pred[0]
    # print(l_pred)

    l_pred = l_pred.argsort()[-3:]

    if model_data.d_name:
        d_model = model_data.load_d_model()
        d_pred = d_model.predict(x)

        d_pred = model_data.stdsc.inverse_transform(d_pred)

    return l_pred, d_pred


def get_related(model_data, tag, difficulty):
    p = model_data.problems[tag]
    idx = (np.abs(np.asarray(p) - difficulty)).argmin()
    return p[idx]


def main():
    # thread doesn't work with current keras version
    api.run(threaded=False)


if __name__ == '__main__':
    main()

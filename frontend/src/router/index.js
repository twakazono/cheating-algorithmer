import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

const base_url = 'http://localhost:5000/api/v1';
const predict_url = base_url + '/predict';
const predict_ja_url = predict_url + '/ja';
const tags_url = base_url + '/tags';
const tags_ja_url = tags_url + '/ja';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    props: {
      predict_url: predict_url,
      tags_url: tags_url,
    },
  },
  {
    path: '/ja',
    name: 'ja',
    component: function() {
      return import('../views/Japanese.vue');
    },
    props: {
      predict_url: predict_ja_url,
      tags_url: tags_ja_url,
    },
  },
  {
    path: '/about',
    name: 'about',
    component: function() {
      return import('../views/About.vue');
    },
  },
];

const router = new VueRouter({
  routes,
});

export default router;

class ProblemData:
    def __init__(self, c, l, d, i):
        self.contest = c
        self.letter = l
        self.difficulty = d
        self.title = i

    def __sub__(self, num):
        return self.difficulty - num

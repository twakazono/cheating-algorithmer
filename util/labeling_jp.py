import random


def main():
    tags = ['dp', 'geo', 'graph']

    with open('data_jp.csv', 'w') as f:
        header = ','.join(['text', 'tag'])
        f.write(header + '\n')
        lines = []
        for tag in tags:
            with open(tag + '.tsv') as rf:
                for line in rf:
                    li = line.strip().replace(',', '')
                    row = li.split('\t')
                    text = ','.join([row[1], tag])
                    lines.append(text)
        random.shuffle(lines)
        for line in lines:
            f.write(line + '\n')


if __name__ == '__main__':
    main()

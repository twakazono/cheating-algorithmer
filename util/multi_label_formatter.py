import random

TAG_LIST = [
    'implementation',
    'math',
    'greedy',
    'dp',
    'data structures',
    'brute force',
    'constructive algorithms',
    'graphs',
    'binary search',
    'sortings'
]


def read_tag_list():
    with open('tags.txt', 'r') as f:
        string = f.readline()
        string = string[1:-1]
        string = string.replace('\'', '')
        tags = string.split(', ')
        return tags


def formatter(tag_list):
    random.seed(2019)
    with open('problems-all.tsv', 'r') as f, \
            open('train_f.csv', 'w') as tf, \
            open('dev_f.csv', 'w') as df, \
            open('test_f.csv', 'w') as ef:
        f.readline()
        header = 'text'
        for tag in tag_list:
            header = ','.join([header, tag])
        header += '\n'
        tf.write(header)
        df.write(header)
        ef.write(header)

        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            row = line.split('\t')
            text = row[4]
            string = row[7][1:-1]
            string = string.replace('\'', '')
            tags = string.split(', ')
            is_labeled = False
            for i, tag in enumerate(tag_list):
                if tag in tags:
                    text = ','.join([text, '1'])
                    is_labeled = True
                else:
                    text = ','.join([text, '0'])
            if not is_labeled:
                continue
            text += '\n'
            v = random.randint(0, 9)
            if v == 8:
                df.write(text)
            elif v == 9:
                ef.write(text)
            else:
                tf.write(text)


def single_label(tag_list):
    random.seed(2019)
    with open('problems-all.tsv', 'r') as f, \
            open('train_s.csv', 'w') as tf, \
            open('dev_s.csv', 'w') as df, \
            open('test_s.csv', 'w') as ef:
        f.readline()
        header = 'text,tags\n'
        tf.write(header)
        df.write(header)
        ef.write(header)

        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            row = line.split('\t')
            text = row[4]
            string = row[7][1:-1]
            string = string.replace('\'', '')
            tags = string.split(', ')
            is_labeled = False
            string = ''
            for i, tag in enumerate(tag_list):
                if tag in tags:
                    string += tag if string == '' else '/' + tag
                    is_labeled = True
            if not is_labeled:
                continue
            text = text + ',' + string + '\n'
            v = random.randint(0, 9)
            if v == 8:
                df.write(text)
            elif v == 9:
                ef.write(text)
            else:
                tf.write(text)


def write_labels_csv(tag_list):
    with open('labels.csv', 'w') as f:
        for tag in tag_list:
            f.write(tag + '\n')


def main():
    # tags = read_tag_list()
    tags = TAG_LIST
    formatter(tags)
    single_label(tags)
    write_labels_csv(tags)


if __name__ == '__main__':
    main()

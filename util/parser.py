import glob

from bs4 import BeautifulSoup


class Problem:
    def __init__(self, path, output):
        self.contest = ''
        self.letter = ''
        self.title = ''
        self.time_limit = ''
        self.memory_limit = ''
        self.main_text = ''
        self.input_specification = 'none'
        self.output_specification = 'none'
        self.tags = []
        self.difficulty = ''
        if self.parse_html(path):
            self.dump(output)

    def parse_html(self, path):
        html = ''
        with open(path, 'rb') as f:
            html = f.read()

        soup = BeautifulSoup(html, 'lxml')

        path_items = path.split('/')
        self.contest = path_items[1]
        self.letter = path_items[2].split('.')[0]
        print(self.contest, self.letter)

        self.title = soup.find(attrs={'class': 'title'}).text
        self.time_limit = soup.find(attrs={'class': 'time-limit'}).text.split(
            'time limit per test')[1]
        self.memory_limit = \
            soup.find(attrs={'class': 'memory-limit'}).text.split(
                'memory limit per test')[1]
        self.main_text = soup.find(attrs={'class': 'header'}).next_sibling.text
        tmp = soup.find(attrs={'class': 'input-specification'})
        if tmp is not None:
            self.input_specification = 'Input'.join(tmp.text.split('Input')[1:])
        tmp = soup.find(attrs={'class': 'output-specification'})
        if tmp is not None:
            self.output_specification = 'Output'.join(
                tmp.text.split('Output')[1:])

        tag_boxes = soup.find_all(attrs={'class': 'tag-box'})
        for tag_box in tag_boxes:
            tag = tag_box.text.strip()
            if tag.startswith('*') and tag != '*special problem':
                self.difficulty = tag
            else:
                self.tags.append(tag)

        self.sanitize_all()

        if self.main_text == '' \
                or self.tags == [] \
                or 'special problem' in self.tags:
            return False
        return True

    def sanitize_all(self):
        self.contest = sanitize(self.contest)
        self.letter = sanitize(self.letter)
        self.title = sanitize(self.title)
        self.time_limit = sanitize(self.time_limit)
        self.memory_limit = sanitize(self.memory_limit)
        self.main_text = sanitize(self.main_text)
        self.input_specification = sanitize(self.input_specification)
        self.output_specification = sanitize(self.output_specification)
        self.difficulty = sanitize(self.difficulty)
        for i in range(len(self.tags)):
            self.tags[i] = sanitize(self.tags[i])

    def dump(self, file):
        txt = '\t'.join([self.contest,
                         self.difficulty,
                         self.input_specification,
                         self.letter,
                         self.main_text,
                         self.memory_limit,
                         self.output_specification,
                         str(self.tags),
                         self.time_limit,
                         self.title])
        file.write(txt + '\n')

    def __str__(self):
        return stringify(self)


def sanitize(string):
    raw = ''.join(
        [i if ord(i) < 128 and (i.isalnum() or i.isspace() or i == '-')
         else ' ' for i in string])

    return ' '.join(raw.split())


def stringify(thingy):
    attributes = dir(thingy)
    res = thingy.__class__.__name__ + '(\n'
    for attr in attributes:
        if attr.startswith('__') and attr.endswith('__'):
            continue
        if callable(getattr(thingy, attr)):
            continue
        res += attr + ' = ' + str(getattr(thingy, attr))
        res += '\n'

    res += ')'
    return res


def main():
    dump_path = 'problems-all.tsv'
    path_list = glob.glob('html/*/*.html')

    ignore_list = ['180/A', '497/D', '524/A', '524/B', '528/E', '675/D',
                   '929/A', '929/B', '929/C', '929/D', '936/E', '949/F',
                   '1089/B', '1089/C', '1089/D', '1089/E', '1089/H', '1089/M',
                   '1090/B', '1090/E', '1090/F', '1090/G', '1090/H', '1090/I',
                   '1090/K', '1090/L']
    ignore_list = list(map(lambda s: 'html/' + s + '.html', ignore_list))

    with open(dump_path, 'w') as f:
        header = '\t'.join(['contest',
                            'difficulty',
                            'input_specification',
                            'letter',
                            'main_text',
                            'memory_limit',
                            'output_specification',
                            'tags',
                            'time_limit',
                            'title'])
        f.write(header + '\n')

        for idx, path in enumerate(path_list):
            if path in ignore_list:
                continue

            print(idx, '/', len(path_list))
            problem = Problem(path, f)


def test():
    path = 'html/1119/A.html'
    problem = Problem(path)
    print(problem)


if __name__ == '__main__':
    main()

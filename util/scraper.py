import os
import re
import urllib.parse
import urllib.request

from bs4 import BeautifulSoup


def download_all():
    # num of pages on the https://codeforces.com/problemset/page/
    num_pages = 55

    for i in range(1, num_pages + 1):
        url = 'https://codeforces.com/problemset/page/{}'.format(i)
        f = urllib.request.urlopen(url)
        soup = BeautifulSoup(f, 'lxml')

        for pages in soup.select('.id a', attrs={'href': re.compile(r'/problemset/problem/[a-zA-Z0-9]*/[a-zA-Z0-9]*')}):
            relative_url = pages['href']
            trunc = '/problemset/problem/'
            pid = relative_url[len(trunc):]
            html_path = 'html/{}'.format(pid) + '.html'

            full_url = urllib.parse.urljoin(url, relative_url)

            if os.path.exists(html_path):
                print('Skip: ', full_url)
                continue

            try:
                print("URL: ", full_url)
                os.makedirs(os.path.dirname(html_path), exist_ok=True)
                urllib.request.urlretrieve(full_url, html_path)

            except Exception as e:
                print('Invalid link: {}'.format(e))


def main():
    download_all()


if __name__ == '__main__':
    main()

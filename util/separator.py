import random


def main():
    random.seed(2019)

    with open('problems-all.tsv') as f, \
            open('train.tsv', 'w') as tf, \
            open('dev.tsv', 'w') as df, \
            open('test.tsv', 'w') as ef:
        header = f.readline()
        tf.write(header)
        df.write(header)
        ef.write(header)

        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            v = random.randint(0, 9)
            if v == 8:
                df.write(line)
            elif v == 9:
                ef.write(line)
            else:
                tf.write(line)


if __name__ == '__main__':
    main()

import pandas as pd


def list_all_tags(df):
    tag_set = set()
    for row in df.values:
        string = row[7][1:-1]
        string = string.replace('\'', '')
        tags = string.split(', ')
        for tag in tags:
            tag_set.add(tag)
    return list(tag_set)


def count_all_tags(df):
    tag_dict = {}
    for row in df.values:
        string = row[7][1:-1]
        string = string.replace('\'', '')
        tags = string.split(', ')
        for tag in tags:
            if tag in tag_dict:
                tag_dict[tag] += 1
            else:
                tag_dict[tag] = 1
    tags = sorted(tag_dict.items(), key=lambda x: x[1], reverse=True)
    return tags


def main():
    problems = pd.read_csv('problems-all.tsv', delimiter='\t')
    tags = list_all_tags(problems)
    tags.sort()
    with open('tags.txt', 'w') as f:
        f.write(str(tags))

    tags = count_all_tags(problems)
    with open('tag_count.txt', 'w') as f:
        for key, val in tags:
            text = str(key) + ': ' + str(val) + '\n'
            f.write(text)


if __name__ == '__main__':
    main()
